#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SOURCE 1
#define HEADER 2

#define MAX_ROWS 50
#define MAX_LETTERS 50

int source_or_header(char tmp[MAX_LETTERS]){

     int i = 0;

     //search the word until dot is found
     while( tmp[i] != '.' && tmp[i] != '\0' ){

          i++;
          //if dot is found
          if( tmp[i] == '.'){

               //if its c/cpp file
               if( tmp[i + 1] == 'c' ){
                    //return source
                    return SOURCE;
               }
               //if its header file
               if( tmp[i + 1] == 'h' ){
                    //return header
                    return HEADER;
               }
          }
     }

     //else do nothing, file is not important
     return 0; 
}

int save_names( char source[MAX_ROWS][MAX_LETTERS], char header[MAX_ROWS][MAX_LETTERS], char path[100] ){

     DIR *dir;

     int s = 0; 
     int h = 0;

     char tmp[MAX_LETTERS]; 

     struct dirent *ent;

     if ((dir = opendir (".")) != NULL) {

          /* print all the files and directories within directory */
          while ((ent = readdir (dir)) != NULL) {

               //gimme the name
               //printf ("%s\n", ent->d_name);

               //store name
               strcpy( tmp,ent->d_name );

               //deside if its source of header file
               //and put it at the respective matrix
               if( source_or_header(tmp) == SOURCE ){
                    strcpy( source[s], tmp );
                    s++;
               }
               if( source_or_header(tmp) == HEADER ){
                    strcpy( header[h], tmp );
                    h++;
               }
          }
          closedir (dir);
     } 
     /* could not open directory */
     else {
          perror ("could not open directory");
          return EXIT_FAILURE;
     }

     return 0;
}

//optional
void print_details(char source[MAX_ROWS][MAX_LETTERS], char header[MAX_ROWS][MAX_LETTERS]){

     printf("\n");

     int i = 0;

     while ( strcmp(source[i] , "0") ){    
          printf("source %d : %s\n", i, source[i]);
          i++;
     }
     if ( strcmp(source[0] , "0") == 0 )
          printf("no source files\n");

     i = 0 ;

     while ( strcmp(header[i] , "0") ){    
          printf("header %d : %s\n", i, header[i]);
          i++;
     }
     if ( !strcmp(header[0] , "0") )
          printf("no header files\n");;

}

void print_objected(char source[MAX_LETTERS] ){

     int j = 0;

     while(source[j] != '.'){
          printf("%c", source[j]);
          j++;
     }   

     printf(".o");
}

void print_head(char source[MAX_ROWS][MAX_LETTERS], char header[MAX_ROWS][MAX_LETTERS], char executable[20], char compiler[5]){

     //OBJS = source1.o main.o
     int i = 0;

     printf("OBJS =");
     //but with .o instead of .c/.cpp
     while( strcmp( source[i],"0" ) ){
          printf(" ");
          print_objected( source[i] );

          i++;
     }
     printf("\n");


     //SOURCE = source1.cpp main.cpp
     i = 0;

     printf("SOURCE =");
     while( strcmp( source[i],"0") ){
          printf(" %s", source[i]);
          i++;
     }
     printf("\n");


     //HEADER = header1.h     
     i = 0;

     printf("HEADER =");
     while( strcmp( header[i],"0") ){
          printf(" %s", header[i]);
          i++;
     }

     printf("\n");
     // OUT = school
     // CC = g++
     // FLAGS = -Wall -g -c

     printf("OUT = %s\n",executable);
     printf("CC = %s\n",compiler);
     printf("FLAGS = -Wall -g -c\n");



     printf("\n");

}

void print_body(char source[MAX_ROWS][MAX_LETTERS]){

     printf("all: $(OBJS)\n\t$(CC) -g $(OBJS) -o $(OUT)\n\n");

//      main.o: main.cpp
//         $(CC) $(FLAGS) main.cpp

     int i = 0;

     while( strcmp(source[i], "0") ){

          print_objected(source[i]);
          printf(": %s\n\t$(CC) $(FLAGS) %s\n\n", source[i], source[i]);
          i++;
     }

}

void print_bottom(){

//     clean:
//         rm -f $(OBJS) $(OUT) *~
// 
//      count:
//         wc $(SOURCE) $(HEADER)

     printf("clean:\n\trm -f $(OBJS) $(OUT) *~\n\ncount:\n\twc $(SOURCE) $(HEADER)\n\n");

     printf("# this Makefile has been created by MakeTheMake a.k.a. MTM program");
     printf("# which has been created by Stelios Ntaveas.");

}

void print_file(char source[MAX_ROWS][MAX_LETTERS], char header[MAX_ROWS][MAX_LETTERS], char executable[20], char compiler[5]){


     print_head(source, header, executable, compiler);

     print_body(source);

     print_bottom();
}

int main(int argc, char** argv){

     char executable[20];
     char compiler[5];
     int answer = -1;
     char path[100];

     if( argc == 3){

          strcpy( compiler, argv[1] );
          strcpy( executable, argv[2] );

          fprintf(stderr,"Data given:");

          fprintf(stderr,"Type of the compiler: %s\n", compiler);
          fprintf(stderr,"Name of the executable: %s\n", executable);

          fprintf(stderr,"Are they corrent? (Any number = Yes, 0 = NO):"); 
          scanf("%d",&answer);
          fprintf(stderr,"\n");
     }
     if( argc != 3 || answer <= 0){



          if( answer == -1)
               fprintf(stderr,"Incomplete Input. Please give the correct data manually:\n");

          fprintf(stderr,"\n");

          fprintf(stderr,"Name of the executable:\n");
          fgets(executable, sizeof executable, stdin);

          do{
               fprintf(stderr,"Define compiler (1 for GCC, 2 for G++):\n");
               scanf("%d",&answer);
          }while( answer != 1 && answer != 2);

          if( answer == 1 )
               strcpy(compiler, "gcc");
          if( answer == 2 )
               strcpy(compiler, "g++");
     }

     fprintf(stderr,"\n");

     char source[MAX_ROWS][MAX_LETTERS];
     char header[MAX_ROWS][MAX_LETTERS];

     int i;

     for( i = 0 ; i < MAX_ROWS ; i++){
               strcpy( source[i] , "0" );
               strcpy( header[i] , "0" );
     }

     save_names(source, header, path);

//      print_details(source, header);

     print_file(source, header, executable, compiler);


     return 0;
}
